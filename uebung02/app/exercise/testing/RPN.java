package app.exercise.testing;

import java.util.Stack;
import java.util.EmptyStackException;
import app.exercise.algebra.*;

/**
 * A solver for equations in reverse polish notation using the Rational class.
 * @author Hans-Georg Sommer
 */
public class RPN {
    public static void main(String args[]) {
        Stack<Rational> s = new Stack<Rational>();
        Long v;

        for (String str : args) {
            try {
                if (str.equals("+")) {
                    s.push((Rational) s.pop().add(s.pop()));
                }
                else if (str.equals("-")) {
                    Rational t = s.pop();
                    s.push((Rational) s.pop().sub(t));
                }
                else if (str.equals("*")) {
                    s.push((Rational) s.pop().mul(s.pop()));
                }
                else if (str.equals("/")) {
                    Rational t = s.pop();
                    s.push((Rational) s.pop().div(t));
                }
                else {
                    try {
                        v = Long.parseLong(str);
                        if (v < 0) {
                            System.out.println("Error: Negative values are not allowed!");
                            return;
                        }
                        s.push(new Rational(v));
                    }
                    catch (NumberFormatException e) {
                            System.out.println("Error: Input isn't an integer or operator!");
                            return;
                    }
                }
            }
            catch (EmptyStackException e) {
                System.out.println("Error: Too many operators!");
                return;
            }
        }

        if (s.size() != 1) {
            System.out.println("Warning: Not enough operators to process all values!");
        }
        System.out.println(s.pop());
    }
}


