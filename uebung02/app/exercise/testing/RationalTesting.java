package app.exercise.testing;

import app.exercise.algebra.*;

/**
 * Very basic test class for Rational and CompRational.
 * @author Hans-Georg Sommer
 */
public class RationalTesting {
    public static void main(String args[]) {
        System.out.println("Default construct Rational a:");
        Rational a = new Rational();
        System.out.println("a: " + a);

        System.out.println("Construct Rational b with single argument (17):");
        Rational b = new Rational(9);
        System.out.println("b: " + b);

        System.out.println("Construct Rational c with two arguments (-5 and 12):");
        Rational c = new Rational(-5, 12);
        System.out.println("c: " + c);

        System.out.println("Construct Rational d with two arguments with common divisor (8 and 24):");
        Rational d = new Rational(8, 24);
        System.out.println("d: " + d);

        System.out.println("Construct Rational e with two arguments with negative denominator (1 and -7):");
        Rational e = new Rational(1, -7);
        System.out.println("e: " + e);

        System.out.println("\nAdd b.reciprocal() to a:");
        a.add(b.reciprocal());
        System.out.println("a: " + a);

        System.out.println("...and now add c:");
        a.add(c);
        System.out.println("a: " + a);

        System.out.println("Subtract c from a again:");
        a.sub(c);
        System.out.println("a: " + a);

        System.out.println("Subtract c from itself:");
        c.sub(c);
        System.out.println("c: " + c);

        System.out.println("Multiply d by b:");
        d.mul(b);
        System.out.println("d: " + d);

        System.out.println("Multiply 1/4 by 2/3:");
        Rational foo = new Rational(1, 4);
        Rational bar = new Rational(2, 3);
        foo.mul(bar);
        System.out.println(foo);

        System.out.println("Divide d by d:");
        d.div(d);
        System.out.println("d: " + d);

        System.out.println("Divide d by a.reciprocal():");
        d.div(a.reciprocal());
        System.out.println("d: " + d);

        System.out.println("a.equals(b):");
        System.out.println(a.equals(b));

        System.out.println("d.equals(d):");
        System.out.println(d.equals(d));

        System.out.println("d.hashCode():");
        System.out.println(d.hashCode());

        System.out.println("e.hashCode():");
        System.out.println(e.hashCode());

        System.out.println("\nConstruct CompRational x (4, 7):");
        CompRational x = new CompRational(2, 7);
        System.out.println("x: " + x);

        System.out.println("Construct CompRational y (3, 9):");
        CompRational y = new CompRational(3, 9);
        System.out.println("y: " + y);

        System.out.println("Construct CompRational z (1, 3):");
        CompRational z = new CompRational(1, 3);
        System.out.println("z: " + z);

        System.out.println("x.compareTo(y): " + x.compareTo(y));
        System.out.println("x.compareTo(z): " + x.compareTo(z));
        System.out.println("y.compareTo(x): " + y.compareTo(x));
        System.out.println("y.compareTo(z): " + y.compareTo(z));
        System.out.println("z.compareTo(x): " + z.compareTo(x));
        System.out.println("z.compareTo(y): " + z.compareTo(y));
        System.out.println("z.compareTo(z): " + z.compareTo(z));
        System.out.print("z.compareTo(null): ");
        System.out.println(z.compareTo(null));
    }
}
