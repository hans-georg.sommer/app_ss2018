package app.exercise.algebra;

/**
 * Interface for arithmetic with fractions.
 * @author Hans-Georg Sommer
 */
public interface Fractional {
    /**
     * @return The numerator of the fraction.
     */
    long getN();

    /**
     * @return The demoninator of the fraction.
     */
    long getD();

    /**
     * Add a fraction to the instance.
     * @param operand Fraction to add.
     */
    Fractional add(Fractional operand);

    /**
     * Substract a fraction from the instance.
     * @param operand Fraction to substract.
     */
    Fractional sub(Fractional operand);

    /**
     * Multiply the instance by a fraction.
     * @param operand Fraction to multiply.
     */
    Fractional mul(Fractional operand);

    /**
     * Divide instance by a fraction.
     * @param operand Fraction to divide by.
     */
    Fractional div(Fractional operand);

    /**
     * Get the negation of the fraction.
     * @return New instance representing the additive inverse.
     */
    Fractional negation();

    /**
     * Get the reciprocal of the fraction.
     * @return New instance representing the multiplicative inverse.
     */
    Fractional reciprocal();
}
