package app.exercise.algebra;

/**
 * Abstract base class for a fraction which serves no purpose but to annoy students.
 * @author Hans-Georg Sommer
 */
public abstract class BasisFraction implements Fractional {
    /**
     * Set numerator and denominator to the given values.
     * @param numerator Numerator
     * @param denominator Denominator
     */
    protected abstract void setND(long numerator, long denominator);

    /**
     * Reduce the fraction and make sure, the denominator ist positive.
     */
    protected abstract void normalize();

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#add(app.exercise.algebra.Fractional)
     */
    public Fractional add(Fractional operand) {
        setND(getN() * operand.getD() + operand.getN() * getD(), getD() * operand.getD());
        return this;
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#sub(app.exercise.algebra.Fractional)
     */
    public Fractional sub(Fractional operand) {
        add(operand.negation());
        return this;
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#mul(app.exercise.algebra.Fractional)
     */
    public Fractional mul(Fractional operand) {
        setND(getN() * operand.getN(), getD() * operand.getD());
        return this;
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#div(app.exercise.algebra.Fractional)
     */
    public Fractional div(Fractional operand) {
        mul(operand.reciprocal());
        return this;
    }
}
