package app.exercise.algebra;

/**
 * Fractional class that is even comparable - much wow! Also adds another layer
 * of indirection, see https://en.wikipedia.org/wiki/Fundamental_theorem_of_software_engineering
 * @author Hans-Georg Sommer
 */
public class CompRational extends Rational implements Comparable<CompRational> {

    /**
     * Default constructor, initializes to a zero-like value.
     */
    public CompRational(){
        super();
    }

    /**
     * Construct a fraction from two integer values.
     * @param numerator The numerator of the fraction.
     * @param denominator The denominator of the fraction.
     */
    public CompRational(long numerator, long denominator) {
        super(numerator, denominator);
    }

    /**
     * Constructor for a fraction representing an integer value.
     * @param numerator Integer value
     */
    public CompRational(long numerator) {
        super(numerator);
    }

    /**
     * Copy constructor.
     * @param other Template for the new instance.
     */
    public CompRational(Fractional other) {
        this(other.getN(), other.getD());
    }

	/**
     * Compare this instance to another fraction.
     * @param other The other fraction.
     * @return a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object.
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(CompRational other) {
        if (other == null) {
            throw new NullPointerException("Cannot compare with null object");
        }
        if (other == this) {
            return 0;
        }
        return Long.compare(getN() * other.getD(), other.getN() * getD());
	}

	/**
     * Compare two fractions.
     * @param a The first fraction.
     * @param b The second fraction.
     * @return a negative integer, zero, or a positive integer as a is less than, equal to, or greater than b.
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
    public static int compare(CompRational a, CompRational b) {
        return a.compareTo(b);
    }
}
