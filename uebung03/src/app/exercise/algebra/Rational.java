package app.exercise.algebra;

/**
 * An actual implementation of all this interface and abstract class stuff...
 * @author Hans-Georg Sommer
 */
public class Rational extends BasisFraction {
    /**
     * The numerator of the fraction.
     */
    private long n;
    /**
     * The denominator of the fraction.
     */
    private long d;

    /**
     * Construct a fraction from two integer values.
     * @param numerator The numerator of the fraction.
     * @param denominator The denominator of the fraction.
     */
    public Rational(long numerator, long denominator) {
        setND(numerator, denominator);
    }

    /**
     * Default constructor, initializes to a zero-like value.
     */
    public Rational() {
        this(0, 1);
    }

    /**
     * Constructor for a fraction representing an integer value.
     * @param numerator Integer value
     */
    public Rational(long numerator) {
        this(numerator, 1);
    }

    /**
     * Copy constructor.
     * @param other Template for the new instance.
     */
    public Rational(Fractional other) {
        this(other.getN(), other.getD());
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.BasisFraction#setND(long, long)
     */
    protected void setND(long numerator, long denominator) {
        n = numerator;
        d = denominator;
        normalize();
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#getN()
     */
    public long getN() {
        return n;
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#getD()
     */
    public long getD() {
        return d;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    public Rational clone() {
        return new Rational(this);
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (d ^ (d >>> 32));
		result = prime * result + (int) (n ^ (n >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rational other = (Rational) obj;
		if (d != other.d)
			return false;
		if (n != other.n)
			return false;
		return true;
	}

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        if (d == 1) {
            return "" + n;
        }
        return n + "/" + d;
    }

	/* (non-Javadoc)
	 * @see app.exercise.algebra.Fractional#negation()
	 */
	public Rational negation() {
        return new Rational(n * -1, d);
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#reciprocal()
     */
    public Rational reciprocal() {
        return new Rational(d, n);
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.Fractional#abs()
     */
    public Rational abs() {
        return new Rational(Math.abs(n), d);
    }

    /**
     * Method to calculate the greatest common divisor of the numerator and the denominator.
     * @return The greatest common divisor.
     */
    private long gcd() {
        long a = Math.abs(n);
        long b = d;
        long r;
        while (b != 0) {
            r = a % b;
            a = b;
            b = r;
        }
        return a;
    }

    /* (non-Javadoc)
     * @see app.exercise.algebra.BasisFraction#normalize()
     */
    protected void normalize() {
        if (d < 0) {
            n *= -1;
            d *= -1;
        }
        long x = gcd();
        if (x > 1) {
            n /= x;
            d /= x;
        }
    }
}
