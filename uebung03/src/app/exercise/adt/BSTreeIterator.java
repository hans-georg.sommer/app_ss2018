package app.exercise.adt;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator class for BSTree
 * @author Hans-Georg Sommer
 *
 * @param <E> The element type of the tree
 */
public class BSTreeIterator<E extends Comparable<E>> implements Iterator<E> {

    /**
     * The corresponding BSTree instance 
     */
    private BSTree tree;

    /**
     * The current node in tree
     */
    private TreeNode<E> current;

    /**
     * Construct a new BSTreeIterator object
     * @param t Tree to work on
     */
    public BSTreeIterator(BSTree<E> t) {
        tree = t;
        current = t.root;
        if (current != null) {
            while (current.left != null) {
                current = current.left;
            }
        }
    }

    /**
     * Check, if there is at least one more element in the tree
     * @return true, if there is at least one more element in the tree, false otherwise
     * @see java.util.Iterator#hasNext()
     */
    public boolean hasNext() {
        return current != null;
    }

    /**
     * Get the next element from the iterator
     * @return The next element in the tree
	 * @throws NoSuchElementException if there is no next element
     * @see java.util.Iterator#next()
     */
    public E next() {
        if (current == null) {
            throw new NoSuchElementException();
        }
        E value = current.getValue();
        if (current.right != null) {
            current = current.right;
            while (current.left != null) {
                current = current.left;
            }
        }
        else {
            while (true) {
                if (current.parent == null) {
                    current = null;
                    break;
                }
                if (current.parent.left == current) {
                    // TODO: possible to avoid duplicate code line?
                    current = current.parent;
                    break;
                }
                current = current.parent;
            }
            
        }
        return value;
    }
}
