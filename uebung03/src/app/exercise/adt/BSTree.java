package app.exercise.adt;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Generic class representing a binary search tree
 * @author Hans-Georg Sommer
 *
 * @param <E> The element type of the tree
 */
public class BSTree<E extends Comparable<E>> extends AbstractCollection<E> {

    /**
     * Current number of elements in the tree
     */
    private int size;

    /**
     * The root node of the tree, null if the tree is empty
     */
    protected TreeNode<E> root;

    /**
     * Construct a new empty tree
     */
    public BSTree() {
        size = 0;
        root = null;
    }

    /**
     * Get the current size of the tree
     * @return The current size (number of elements) in the tree
     * @see java.util.AbstractCollection#size()
     */
    public int size() {
        return size;
    }

    /**
     * Check if the tree is empty
     * @return true, if the tree contains zero elements
     * @see java.util.AbstractCollection#isEmpty()
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Get the root element of the tree
     * @return the root element of the tree
     */
    public TreeNode getRoot() {
        return root;
    }

    /**
     * Check, if an object is contained in the tree
     * @param o Object to search for
     * @return true, if the tree contains o, false otherwise or if o == null
     * @see java.util.AbstractCollection#contains(java.lang.Object)
     */
    public boolean contains(Object o) {
        if (o == null || size == 0) {
            return false;
        }
        TreeNode<E> node = binarySearch(o);
        return node != null && node.getValue().equals(o);
    }

    /**
     * Check, if every element of a Collection is in the tree
     * @param c Collection containing the elements to check for
     * @return true, if every element if c is also in the tree, false otherwise
     * @see java.util.AbstractCollection#containsAll(java.util.Collection)
     */
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Convert tree to an array
     * @return Array of type Object containing all tree elements
     * @see java.util.AbstractCollection#toArray()
     */
    public Object[] toArray() {
        // copy-pasted from https://docs.oracle.com/javase/7/docs/api/java/util/AbstractCollection.html#toArray()
        List<E> list = new ArrayList<E>(size());
        for (E e : this) {
            list.add(e);
        }
        return list.toArray();
    }

    /**
     * Get string representation of the tree
     * @return String representation of the tree
     * @see java.util.AbstractCollection#toString()
     */
    public String toString() {
        String s = "";
        for (E e : this) {
            s += e + " ";
        }
        return s;
    }

    /**
     * Add a new element to the tree
     * @param e Element to add
     * @return true, if the element was added to the tree, false if there was an error or e was already in the tree
     * @see java.util.AbstractCollection#add(java.lang.Object)
     */
    public boolean add(E e) {
        TreeNode<E> n = new TreeNode<E>(e);
        if (root == null) {
            root = n;
            ++size;
            return true;
        }
        else {
            TreeNode<E> node = binarySearch(e);
            if (node.getValue().compareTo(e) < 0) {
                n.parent = node;
                node.right = n;
                ++size;
                return true;
            }
            else if (node.getValue().compareTo(e) > 0) {
                n.parent = node;
                node.left = n;
                ++size;
                return true;
            }
            else {
                // value already in the tree
                return false;
            }
        }
    }

    /**
     * Remove an element from the tree
     * @param o Object to remove
     * @return true, if an element was removed, false, if there was an error or o wasn't in the tree
     * @see java.util.AbstractCollection#remove(java.lang.Object)
     */
    public boolean remove(Object o) {
        if (o == null || size == 0) {
            return false;
        }
        TreeNode<E> node = binarySearch(o);
        if (node == null) {
            return false;
        }
        if (node.getValue().equals(o)) {
            if (node.left == null) {
                // node has no childs or only right child
                if (node.right != null) {
                    node.right.parent = node.parent;
                }
                if (node == root) {
                    root = node.right;
                }
                else {
                    if (node == node.parent.left) {
                        node.parent.left = node.right;
                    }
                    else {
                        node.parent.right = node.right;
                    }
                }
            }
            else if (node.right == null) {
                // node has only left child
                if (node == root) {
                    root = node.left;
                    root.parent = null;
                }
                else {
                    node.left.parent = node.parent;
                    if (node == node.parent.left) {
                        node.parent.left = node.left;
                    }
                    else {
                        node.parent.right = node.left;
                    }
                }
            }
            else {
                // node has two childs
                TreeNode<E> pre = node.left;
                if (pre.right == null) {
                    node.left = pre.left;
                }
                else {
                    while (pre.right != null) {
                        pre = pre.right;
                    }
                    pre.parent.right = null;
                }
                node.setValue(pre.getValue());
                if (node.left != null) {
                    node.left.parent = node;
                }
            }
            --size;
            return true;
        }
        return false;
    }

    /**
     * Get a nwe iterator instance for the tree
     * @return A new iterator to iterate through the elements in the tree
     * @see java.util.AbstractCollection#iterator()
     */
    public Iterator<E> iterator() {
        return new BSTreeIterator<E>(this);
    }

    /**
     * Get the minimum value stored in the tree
     * @return The minimum (leftmost) value in the tree
     */
    public E min() {
        if (root == null) {
            return null;
        }
        TreeNode<E> current = root;
        while (current.left != null) {
            current = current.left;
        }
        return current.getValue();
    }

    /**
     * Get the maximum value stored in the tree
     * @return The maximum (rightmost) value in the tree
     */
    public E max() {
        if (root == null) {
            return null;
        }
        TreeNode<E> current = root;
        while (current.right != null) {
            current = current.right;
        }
        return current.getValue();
    }

    /**
     * Binary search the tree for a given value
     * @param o Value to search for
     * @return Either the tree node containing o or the position, where o would be, if o isn't in the tree
     */
    private TreeNode<E> binarySearch(Object o) {
        TreeNode<E> current = root;
        E val;
        try{
            val = (E) o;
        }
        catch (ClassCastException e) {
            return null;
        }
        while (true) {
            // TODO: is it possible to simplify this?
            if (current.getValue().compareTo(val) < 0) {
                if (current.right == null) {
                    return current;
                }
                else {
                    current = current.right;
                }
            }
            else if (current.getValue().compareTo(val) > 0) {
                if (current.left == null) {
                    return current;
                }
                else {
                    current = current.left;
                }
            }
            else {
                return current;
            }
        }
    }
}
