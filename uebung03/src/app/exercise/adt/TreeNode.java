package app.exercise.adt;

import app.exercise.visualtree.DrawableTreeElement;

/**
 * Class representinng a single Node in a tree.
 * @author Hans-Georg Sommer
 *
 * @param <T> The element type of the node
 */
public class TreeNode<T extends Comparable<T>> implements DrawableTreeElement<T> {

    /**
     * The value stored in the node
     */
    private T value;

    /**
     * References to the left and the right child node and also the parent or null, if they do not existent.
     */
    protected TreeNode<T> left, right, parent;

    /**
     * Constructor for a node
     * @param val The value to be stored in the node
     */
    public TreeNode(T val) {
        value = val;
        left = right = parent = null;
    }

    /**
     * Get the value of the node
     * @return The value of the node
     * @see app.exercise.visualtree.DrawableTreeElement#getValue()
     */
    public T getValue() {
        return value;
    }

    /**
     * Set the nodes value
     * @param v The new value for the node
     */
    public void setValue(T v) {
        value = v;
    }

    /**
     * Dummy function to satisfy DrawableTreeElement requirements
     * @return Alsways false
     * @see app.exercise.visualtree.DrawableTreeElement#isRed()
     */
    public boolean isRed() {
        return false;
    }

    /**
     * Get the left child element
     * @return The left child element or null, if there is no left child
     * @see app.exercise.visualtree.DrawableTreeElement#getLeft()
     */
    public DrawableTreeElement<T> getLeft() {
        return left;
    }

    /**
     * Get the right child element
     * @return The right child element or null, if there is no right child
     * @see app.exercise.visualtree.DrawableTreeElement#getLeft()
     */
    public DrawableTreeElement<T> getRight() {
        return right;
    }

    /**
     * Get the parent element
     * @return The parent element or null if this is the root node
     * @see app.exercise.visualtree.DrawableTreeElement#getLeft()
     */
    public DrawableTreeElement<T> getParent() {
        return parent;
    }
}
