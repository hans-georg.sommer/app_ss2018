package app.exercise.testing;

import app.exercise.adt.BSTree;
import app.exercise.adt.BSTreeIterator;
import app.exercise.algebra.CompRational;
import app.exercise.visualtree.RedBlackTreeDrawer;
import java.util.Random;

/**
 * Test class for a binary search tree
 * @author Hans-Georg Sommer
 *
 */
public class BSTreeTester {

	/**
	 * RedBlackTreeDrawer instance to draw the tree
	 */
	private static RedBlackTreeDrawer<CompRational> visual;

    /**
     * BSTree instance containing the data
     */
    private static BSTree<CompRational> primary;

	/**
	 * Convenience function to redraw the tree and pause the program afterwards
	 * @throws InterruptedException
	 */
	private static void draw() throws InterruptedException {
		visual.draw(primary.getRoot());
		Thread.sleep(1000);
	}

    /**
     * The almighty main function
     * @param args Command line arguments
     * @throws InterruptedException
     */
    public static void main(String args[]) throws InterruptedException {
        if (args.length == 0) {
            System.out.println("Usage: $JAVAFOO n1 d1 [n2 d2 n3 d3 ...]");
            return;
        }
        if (args.length % 2 == 1) {
            System.out.println("Error: odd number of arguments");
            return;
        }
        CompRational[] input = new CompRational[args.length / 2];
        for (int i = 0; i < args.length; i += 2) {
            try {
                int a = Integer.parseInt(args[i]);
                int b = Integer.parseInt(args[i+1]);
                input[i/2] = new CompRational(a, b);
            }
            catch (NumberFormatException e) {
                System.out.println("Input not parseable as integer.");
                return;
            }
        }

		primary = new BSTree<CompRational>();
		BSTree<CompRational> secondaryEven = new BSTree<CompRational>();
		BSTree<CompRational> secondaryOdd = new BSTree<CompRational>();
		visual = new RedBlackTreeDrawer<CompRational>();

        for (int i = 0; i < input.length; ++i) {
            primary.add(input[i]);
            if (i % 2 == 0) {
                secondaryEven.add(input[i]);
            }
            else {
                secondaryOdd.add(input[i]);
            }
            draw();
        }

        System.out.println("Content of the primary tree:");
        System.out.println(primary);

        System.out.println("Content of the first secondary tree:");
        System.out.println(secondaryEven);

        System.out.println("Content of the second secondary tree:");
        System.out.println(secondaryOdd);

        System.out.println("The primary tree contains all elements of the first secondary tree:");
        System.out.println(primary.containsAll(secondaryEven));
        System.out.println("The primary tree contains all elements of the second secondary tree:");
        System.out.println(primary.containsAll(secondaryOdd));
        System.out.println("The primary tree contains the first input value (" + input[0] + "):");
        System.out.println(primary.contains(input[0]));
        System.out.println("The primary tree contains the last input value (" + input[input.length-1] + "):");
        System.out.println(primary.contains(input[input.length-1]));

        primary.remove(input[0]);
		draw();
        primary.remove(input[input.length-1]);
		draw();

        if (primary.size() < 2) {
            System.out.println("The primary Tree has at most one element, skipping Random part.");
            return;
        }
        System.out.println("\nRandom values:");
        CompRational min = new CompRational(primary.min());
        CompRational max = new CompRational(primary.max());
        Random rng = new Random();
        CompRational c;
        for (int i = 0; i < 100; ++i) {
            while (true) {
                c = new CompRational(rng.nextLong(), rng.nextLong());
                // brute-force check to ensure random values are between min and max of the tree
                if ((double) min.getN() / min.getD() <= (double) c.getN() / c.getD() &&
                    (double) max.getN() / max.getD() >= (double) c.getN() / c.getD()) {
                    break;
                }
            }
            if (primary.contains(c)) {
                primary.remove(c);
				draw();
                System.out.println("Removed " + c + " from the primary tree.");
            }
            else {
                System.out.println(c + " not found in the primary tree.");
            }
        }
    }
}
