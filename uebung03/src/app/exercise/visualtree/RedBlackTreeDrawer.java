package app.exercise.visualtree;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

public class RedBlackTreeDrawer<T> extends JFrame {
    private static final long serialVersionUID = 1L;
    private RedBlackTreePanel<T> panel;

    public RedBlackTreeDrawer() {
        panel = new RedBlackTreePanel<T>();
        panel.setPreferredSize(new Dimension(800, 600));
        add(panel);

        Action closeAction = new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                //~ dispose(); // this does not terminate the main function
                System.exit(0);
            }
        };

        KeyStroke closeKey = KeyStroke.getKeyStroke(KeyEvent.VK_Q, 0);
        panel.getInputMap().put(closeKey, "close");
        panel.getActionMap().put("close", closeAction);

        addComponentListener(new ComponentAdapter() {
          public void componentResized(ComponentEvent e) {
            Container content = getContentPane();
            panel.setPreferredSize(new Dimension(content.getWidth(), content.getHeight()));
            panel.repaint();
            }
        });
        pack();

        setTitle("Bäumchen-Betrachter");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        }

    public void draw(DrawableTreeElement<T> root) {
        panel.draw(root);
    }

    public void drawDebug(DrawableTreeElement<T> root) {
        panel.drawDebug(root);
    }
}
