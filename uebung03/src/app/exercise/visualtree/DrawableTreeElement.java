package app.exercise.visualtree;

public abstract interface DrawableTreeElement<T> {
    public abstract DrawableTreeElement<T> getParent();
    public abstract DrawableTreeElement<T> getLeft();
    public abstract DrawableTreeElement<T> getRight();
    public abstract boolean isRed();
    public abstract T getValue();
}
