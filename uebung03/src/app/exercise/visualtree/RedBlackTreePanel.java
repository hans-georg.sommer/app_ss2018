package app.exercise.visualtree;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

public class RedBlackTreePanel<T> extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int MIN_DEPTH = 4;
    private static final int NODE_SIZE = 50;
    DrawableTreeElement<T> node;

    static enum flag {
        NORMAL, DEBUG;
        private flag() {}
    }
    
    private flag f = flag.NORMAL;

    public RedBlackTreePanel() {
        setDoubleBuffered(true);
        node = null;
    }

    public void draw(DrawableTreeElement<T> root) {
        node = root;
        f = flag.NORMAL;
        repaint();
    }

    public void drawDebug(DrawableTreeElement<T> root) {
        node = root;
        f = flag.DEBUG;
        repaint();
    }

    public void paint(Graphics gOld) {
        Graphics2D g = (Graphics2D)gOld;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        drawTree(g);
    }

    private void drawTree(Graphics2D g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());

        int depth = getDepth(node);
        Dimension d = getPreferredSize();
        int width = (int)d.getWidth();
        int height = (int)d.getHeight();

        int size = 50;
        while (size > Math.pow(0.5D, depth - 1) * width) {
            size--;
        }
        int steps = depth > 4 ? depth : 4;

        drawSubTree(g, node, 0, width, height / (steps + 1), height / (steps + 1), size);
    }

    private void drawSubTree(Graphics2D g, DrawableTreeElement<T> node, int xMin, int xMax, int yLayer, int yLayerStep, int size) {
        int x = xMin + (xMax - xMin) / 2;
        int y = yLayer;

        if (node != null) {
            int x1 = xMin + (xMax - xMin) / 4;
            int x2 = xMin + (xMax - xMin) * 3 / 4;
            int y12 = y + yLayerStep;

            g.setColor(Color.BLACK);
            g.setStroke(new BasicStroke(2.0F));
            g.drawLine(x, y, x1, y12);
            g.drawLine(x, y, x2, y12);

            drawSubTree(g, node.getLeft(), xMin, xMin + (xMax - xMin) / 2, yLayer + yLayerStep, yLayerStep, size);
            drawSubTree(g, node.getRight(), xMin + (xMax - xMin) / 2, xMax, yLayer + yLayerStep, yLayerStep, size);
        }
        drawNode(g, x, y, size, node != null ? node.getValue() : null, node != null ? node.isRed() : false);

        if ((f == flag.DEBUG) && (node != null)) {
            drawDebugElements(g, x, y, size,
                    node.getParent() == null ? null : node.getParent().getValue(),
                    node.getLeft() == null ? null : node.getLeft().getValue(),
                    node.getRight() == null ? null : node.getRight().getValue()
            );
        }
    }

    private int getDepth(DrawableTreeElement<T> node) {
        if (node == null)
            return 1;
        int left = getDepth(node.getLeft());
        int right = getDepth(node.getRight());
        return 1 + (left > right ? left : right);
    }

    private void drawNode(Graphics2D g, int x, int y, int size, T value, boolean red) {
        boolean leaf = value == null;
        int fontSize = size / 3;

        if (leaf) {
            g.setColor(Color.BLACK);
            g.fillRect(x - size / 2, y - 2 * size / 6, size, 2 * size / 3);
        }
        else {
            g.setColor(red ? Color.RED : Color.BLACK);
            g.fillOval(x - size / 2, y - size / 2, size, size);
            g.setColor(Color.BLACK);
            g.drawOval(x - size / 2, y - size / 2, size, size);
        }

        String text = value != null ? value.toString() : "null";
        Font font;
        int textWidth; int textHeight;
        do {
            font = new Font("Dialog", 1, fontSize);
            FontMetrics metrics = g.getFontMetrics(font);
            textWidth = metrics.stringWidth(text);
            textHeight = metrics.getAscent();
            fontSize--;
        } while (textWidth > 0.8D * size);

        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(text, x - textWidth / 2, y + textHeight / 2);
    }

    private void drawDebugElements(Graphics2D g, int x, int y, int size, T vfather, T vleft, T vright){
        int fontSizef = size / 3;
        int fontSizel = size / 3;
        int fontSizer = size / 3;
        String svf = vfather == null ? "null" : vfather.toString();
        String svl = vleft == null ? "null" : vleft.toString();
        String svr = vright == null ? "null" : vright.toString();

        Font fontf;
        int textWidthf, textHeightf;
        do {
            fontf = new Font("Dialog", 1, fontSizef);
            FontMetrics metrics = g.getFontMetrics(fontf);
            textWidthf = metrics.stringWidth(svf);
            textHeightf = metrics.getAscent();
            fontSizef--;
        } while (textWidthf > 0.4D * size);

        Font fontl;
        int textWidthl, textHeightl;
        do {
            fontl = new Font("Dialog", 1, fontSizel);
            FontMetrics metrics = g.getFontMetrics(fontl);
            textWidthl = metrics.stringWidth(svl);
            textHeightl = metrics.getAscent();
            fontSizel--;
        } while (textWidthl > 0.4D * size);

        Font fontr;
        int textWidthr, textHeightr;
        do {
            fontr = new Font("Dialog", 1, fontSizer);
            FontMetrics metrics = g.getFontMetrics(fontr);
            textWidthr = metrics.stringWidth(svr);
            textHeightr = metrics.getAscent();
            fontSizer--;
        } while (textWidthr > 0.4D * size);


        g.setColor(Color.BLACK);

        g.setFont(fontf);
        g.drawString(svf, x - textWidthf / 2, y + textHeightf / 2 - 2 * size / 3);

        g.setFont(fontl);
        g.drawString(svl, x - textWidthl / 2 - 2 * size / 3, y + textHeightl / 2 + size / 3);

        g.setFont(fontr);
        g.drawString(svr, x - textWidthr / 2 + 2 * size / 3, y + textHeightr / 2 + size / 3);
    }
}
