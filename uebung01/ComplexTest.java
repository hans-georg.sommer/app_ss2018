public class ComplexTest {
    public static void prettyComplex(Complex a) {
        System.out.print("[" + a);
        System.out.print(" real: " + a.getReal() + " imaginary: " + a.getImaginary());
        System.out.println(" abs: " + a.abs() + " hash: " + a.hashCode() + "]");
    }

    public static void main(String args[]) {
        System.out.println("Static class members:");
        System.out.print("Complex.ZERO:\t");
        prettyComplex(Complex.ZERO);
        System.out.print("Complex.ONE:\t");
        prettyComplex(Complex.ONE);
        System.out.print("Complex.I:\t");
        prettyComplex(Complex.I);

        System.out.println("\nDefault construct Complex a");
        Complex a = new Complex();
        System.out.println("a: " + a);

        System.out.println("\nConstruct Complex b from Complex.ONE");
        Complex b = new Complex(Complex.ONE);
        System.out.println("b: " + b);

        System.out.println("\nTest setter and getter on a:");
        a.setReal(23);
        a.setImaginary(42);
        System.out.println("a: " + a);
        System.out.println("a.getReal(): " + a.getReal());
        System.out.println("a.getImaginary(): " + a.getImaginary());

        System.out.println("\nComparison tests:");
        System.out.println("b.equals(Complex.ONE): " + b.equals(Complex.ONE));
        System.out.println("b.equals(b): " + b.equals(b));
        System.out.println("b.equals(a): " + b.equals(a));
        System.out.println("b == Complex.ONE: " + (b == Complex.ONE));

        System.out.println("\nAddition tests:");
        System.out.print("b.add(Complex.ONE): ");
        b.add(Complex.ONE);
        prettyComplex(b);
        System.out.println("Complex.add(Complex.ONE, Complex.I): " + Complex.add(Complex.ONE, Complex.I));
        System.out.println("Complex.add(a, b): " + Complex.add(a, b));
        System.out.print("a: ");
        prettyComplex(a);
        System.out.print("b: ");
        prettyComplex(b);

        System.out.println("\nMultiplication tests:");
        System.out.print("a.mult(b): ");
        a.add(b);
        prettyComplex(a);
        System.out.println("Complex.mult(Complex.ONE, Complex.I): " + Complex.mult(Complex.ONE, Complex.I));
    }
}

