/**
 * Class representing complex numbers.
 * @author Hans-Georg Sommer
 */
public class Complex {

	/**
	 * The real component of the complex number.
	 */
	private double real;

    /**
     * The imaginary component of the complex number.
     */
    private double imaginary;

    /**
     * Default constructor.
     */
    Complex() {
        this(0.0, 0.0);
    }

    /**
     * Construct Complex given the individual components.
     * @param r The real component.
     * @param i The imaginary component.
     */
    Complex(double r, double i) {
        real = r;
        imaginary = i;
    }

    /**
     * Copy constructor.
     * @param other Complex instance to copy.
     */
    Complex(Complex other) {
        this(other.real, other.imaginary);
    }

    /**
     * Complex number 0 + 0i
     */
    public static final Complex ZERO = new Complex(0, 0);

    /**
     * Complex number 1 + 0i
     */
    public static final Complex ONE = new Complex(1, 0);

    /**
     * Complex number 0 + 1i
     */
    public static final Complex I = new Complex(0, 1);

    /**
     * Get the real component.
     * @return The real component.
     */
    public double getReal() {
        return real;
    }

    /**
     * Get the imaginary component.
     * @return The imaginary component.
     */
    public double getImaginary() {
        return imaginary;
    }

    /**
     * Set the real component.
     * @param r New value for real component.
     */
    public void setReal(double r) {
        real = r;
    }

    /** Set the imaginary component.
     * @param i New value for imaginary component.
     */
    public void setImaginary(double i) {
        imaginary = i;
    }

    /**
     * Calculate absolute value.
     * @return Absolute value of the complex number.
     */
    public double abs() {
        return Math.sqrt(real * real + imaginary * imaginary);
    }

    /**
     * Calculate absolute value
     * @param a Complex instance to calculate absolute value for.
     * @return Absolute value of the given complex number.
     */
    public static double abs(Complex a) {
        return a.abs();
    }

    /**
     * Add a complex number to the current instance.
     * @param other Complex number to add.
     */
    public Complex add(Complex other) {
        real += other.real;
        imaginary += other.imaginary;
        return this;
    }

    /**
     * Add two complex numbers.
     * @param a First summand.
     * @param b Second summand.
     * @return The sum of a and b.
     */
    public static Complex add(Complex a, Complex b) {
        Complex c = a.clone();
        c.add(b);
        return c;
    }

    /**
     * Multiply a complex number to the current instance.
     * @param other Complex number to multiply.
     */
    public Complex mult(Complex other) {
        real = real * other.real - imaginary * other.imaginary;
        imaginary = real * other.imaginary + other.real * imaginary;
        return this;
    }

    /**
     * Multiply two complex numbers.
     * @param a First factor.
     * @param b Second factor.
     * @return Product of a and b.
     */
    public static Complex mult(Complex a, Complex b) {
        Complex c = a.clone();
        c.mult(b);
        return c;
    }

    /**
     * Create a copy of the current instance.
     */
    public Complex clone() {
        return new Complex(this);
    }

    /**
     * Check if another complex number has the same value.
     * @param obj Object to compare.
     * @return True if obj is equal to this, false otherwise.
	 * @see java.lang.Object#equals(java.lang.Object)
     */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Complex other = (Complex) obj;
		if (imaginary != other.imaginary)
			return false;
		if (real != other.real)
			return false;
		return true;
	}

    /**
     * Compare two complex numbers.
     * @param a First complex number.
     * @param b Second complex number.
     * @return True if a and b are equal, false otherwise, 
     */
    public static boolean equals(Complex a, Complex b) {
        return a.equals(b);
    }

	/**
	 * Compute hash value for the current instance.
     * @return Hash value of the instance.
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

    /**
     * Compute hash value for a Complex object.
     * @param a Complex instance to hash.
     * @return Hash value of a.
     */
    public static int hashCode(Complex a) {
        return a.hashCode();
    }

    /**
     * Create a string representation.
     * @return String representation of the complex number.
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return Double.toString(real) + "+" + Double.toString(imaginary) + "i";
    }
}
